# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Eslint config
- Base HTML and CSS
- Cube renderer
- This CHANGELOG
- A README
- A CONTRIBUTING guide

[Unreleased]: https://gitlab.com/rbdr/flower/compare/master...develop
