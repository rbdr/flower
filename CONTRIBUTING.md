# Contributing to Flower

This was a project I did for fun, I have no intention of updating it. While
contributions are currently not expected, any input, improvement or help is
welcome and appreciated.

## The objective of Flower

To draw a pretty flower

## How to contribute

Above All: Be nice, always.

* Ensure the linter shows no warnings or errors
* Don't break the CI
* Make the PRs according to [Git Flow][gitflow]: (features go to
  develop, hotfixes go to master)

[gitflow]: https://github.com/nvie/gitflow
